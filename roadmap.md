# Roadmap

A summary of higher level goals:

In terms of dependencies, these upper-levels can all be developed more-or-less in parallel; there may be some overlap in terms of `remote-execution`. However, the priorities should be `rules_buildstream` and `minimal-runtime`. the bulk of the effort is expected to be in developing and upstreaming the buildstream rules

## `bazelize-plugin`
### `bazelize-plugin-dev`
further development efforts: efficiency, better handling of sources, additional support for rules, etc

## `rules_buildstream`
### `rules_buildstream-demo`
  - demo rules\_buildstream used in example project
  - the rules will need to be packaged in an appropriate way for use by an external bazel project
  - these should be converted to repository rules and bolt-on a setup rule for use as a non-hermetic in the bazel context
  - rules\_buildstream will need to expose `bst build` at this stage
### `rules_buildstream-dev`
- api for providing configuration to bst commands (eg. `-o arch x86_32` supplied to `bst build`)
- api for subcommands for (eg. `bst source track`)
- mechanism for source tracking via `bst source track`
- integration with the bazelize plugin

### `rules_buildstream-future`
development of convenience featutres:
  - templating or full generation of bst projects via `rules\_buildstream`? provide additional features in the rules that satisfy all of `rules_buildstream-upstream` but completely via bazel (eg. for specifying tags to track on sources without having to write element declarations)
  - there needs to be consideration for bazel users in terms of ease of adoption

## `minimal-runtime`
### `minimal-runtime-definition`
  - declare a reproducible client runtime which represents a minimal runtime for both bazel and buildstream clients (this can be based on the work in https://gitlab.com/celduin/buildstream-bazel/bazel-toolchains-fdsdk and should target x86_32 and aarch64)
  - produce and automatically validate the client runtime oci-compliant image
  - integrate the runtime with the bazelize plugin for linking
  - provide distribution for the image

## `buildstream-to-bazel`
- a good usecase is required to motivate further development, the major value is ability to build non-bazel packages from source
- given a rich enough rules\_buildstream api and good integration with bazelize, source transforms such as [bazel-source](https://gitlab.com/BuildStream/bst-plugins-experimental/-/blob/master/src/bst_plugins_experimental/sources/bazel_source.py) should be unnecessary.
- CAS sharing between clients out-of-context (https://gitlab.com/celduin/buildstream-bazel/issue-tracker/-/issues/24#note_330873164)

## `remote-execution`
- demonstrate artifact and cache sharing between clients
- there is ongoing work in buildbarn for buildstream support and in celduin-infra for [cache sharing](https://gitlab.com/celduin/infrastructure/celduin-infra/-/merge_requests/107), this should be (more-or-less) consumable in example projects

## Upstreaming Efforts
### `bazelize-plugin-upstream`
upstream to bst-plugins-experimental

### `minimal-runtime-upstream`
implement a subset of the minimal rootfs as an additional fdsdk split and propose to upstream freedesktop-sdk

### `rules_buildstream-upstream`

- Propose `rules_buildstream` as a project in the [bazelbuild](https://github.com/bazelbuild/) namespace. This should be acceptable especially if we can demonstrate a good usecase.

- integrate into existing project (such as tensorflow or apollo) as a replacement for the third_party stack and get upstream adoption. The value to the project is deferring the management of the stack to buildstream instead of manually updating package BUILDs (see !38405). As a first effort on a collection of third party archives we could imagine:
  - 1: replace individual tarballs with a stack tarball built by buildstream (even if it simply imports and composes those tarballs itself) and replace each rule with a single `third_party_http_archive` call specifying that project artifact - this can also be a partial effort
  - 2: use the bazelize plugin to generate the rule call via a bazelize element on top of the compose element
  - 3: replace the `third_party_http_archive` rule with a `rules\_buildstream` rule: this will build the composition of third-party dependencies and position them as a package with a generated BUILD file in the bazel project.
